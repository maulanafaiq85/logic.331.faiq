package Array2D;

import java.util.Scanner;
public class Example11 {
    public static void sebelas(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input N :");
        int n = input.nextInt();
        int baris = 0;
        String simbol = "*";

        String[][] results = new String[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < (n-(i+1)); j++) {
                results[i][baris] = " ";
                baris += 1;
            }
            for (int k = 0; k < (i+1); k++) {
                results[i][baris] = simbol;
                baris += 1;
            }
            baris = 0;
        }
        Utility.PrintArray2DString(results);
    }
}

