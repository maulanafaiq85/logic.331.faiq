package Array2D;

import java.util.Scanner;

public class Example5 {
    public static void lima() {
        Scanner input = new Scanner(System.in);
        System.out.print("Input N :");
        int n = input.nextInt();

        int[][] results = new int[2][n];

        // Print the header row
        System.out.print("n = " + n + "\n");
        for (int i = 0; i < 7; i++) {
            System.out.print(i + "\t");
        }
        System.out.println();

        // Generate and print the sequence
        for (int row = 0; row < 2; row++) {
            for (int i = 0; i < 7; i++) {
                System.out.print(n + "\t");
                n++;
            }
            System.out.println();
        }
    }
}

