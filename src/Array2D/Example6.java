package Array2D;

import java.util.Scanner;

public class Example6 {
    public static void enam(){
        Scanner input = new Scanner(System.in);
        int baris1 = 0;
        int baris2 = 1;
        int baris3 = 0;

        System.out.print("Input N :");
        int n = input.nextInt();

        int[][] results = new int[3][n];

        for (int i = 0; i < 3; i++){
            for (int j = 0; j < n; j++){
                if (i == 0)
                {
                    results[i][j] = baris1;
                    baris1 += 1;
                }
                else if (i == 1)
                {
                    results[i][j] = baris2;
                    baris2 *= 7;
                }
                else if (i == 2)
                {
                    baris3= results[0][j] + results[1][j];
                    results[i][j] = baris3;
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}


