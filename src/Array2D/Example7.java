package Array2D;
import java.util.Scanner;
public class Example7 {
    public static void tujuh(){
        Scanner scanner = new Scanner(System.in);

        int baris = 3;

        System.out.print("Input N :");
        int kolom = scanner.nextInt();

        int[][] results = new int[baris][kolom];

        int value = 0;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < kolom; j++) {
                results[i][j] = value;
                value++;
            }
        }
        Utility.PrintArray2D(results);
    }
}

