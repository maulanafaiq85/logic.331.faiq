package Array2D;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;

        System.out.println("Pilih soal (1 - 12)");
        System.out.print("Masukkan Pilihan =");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12)
        {
            System.out.print("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                Example.Ggenap();
                break;
            case 2:
                Example2.minus();
                break;
            case 3:
                Example3.dua();
                break;
            case 4:
                Example4.empat();
                break;
            case 5:
                Example5.lima();
                break;
            case 6:
                Example6.enam();
                break;
            case 7:
                Example7.tujuh();
                break;
            case 8:
                Example8.delapan();
                break;
            case 9:
                Example9.sembilan();
                break;
            case 10:
                Example10.sepuluh();
                break;
            case 11:
                Example11.sebelas();
                break;
            case 12:
                break;
            default:
        }
    }
}