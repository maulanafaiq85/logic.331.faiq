package Array2D;

public class Utility {
    public static void PrintArray2D(int[][] results){
        for (int i = 0; i < results.length; i++){
            for(int j = 0; j < results[0].length; j++){
                System.out.print(results[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void PrintArray2DString(String[][] resolve){
        for (int i = 0; i < resolve.length; i++) {
            for (int j = 0; j < resolve[i].length; j++) {
                System.out.print(resolve[i][j]);
            }
            System.out.println();
        }
    }
}
