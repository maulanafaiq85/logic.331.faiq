package Basic;

import java.util.Scanner;

public class Lingkaran {

    private static Scanner input = new Scanner(System.in);

    private static double jarijari;
    private static double diameter = jarijari * 2;
    private static double phi = 3.14;

    public static void luas()
    {
        // luas & keliling lingkaran
        System.out.println("Masukan ukuran jari jari (CM)");
        jarijari = input.nextDouble();

        System.out.println("Luas lingkaran = " + (phi * (jarijari * jarijari) ));
    }

    public static void keliling()
    {
        System.out.println("Masukan ukuran jari jari (CM)");
        jarijari = input.nextDouble();

        System.out.println("Keliling Lingkaran = " +(2 * phi * jarijari));
    }
}