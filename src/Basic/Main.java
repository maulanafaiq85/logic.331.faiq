package Basic;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean flag = true;
        String answer = "y";
        int panjang, lebar, alas, tinggi, bidang, alas1, alas2, jari;
        while(flag){
            System.out.println("Menu : ");
            System.out.println("1. persegi panjang");
            System.out.println("2. segitiga");
            System.out.println("3. trapesium");
            System.out.println("4. Lingkaran");
            System.out.println("5. Persegi");
            System.out.print("Pilihan anda : ");

            String prompt = "Pilihan 1. Luas 2. Keliling";
            int pilihan1 = input.nextInt();
            int pilihan2;
            switch (pilihan1) {
                case 1:
                    System.out.println(prompt +
                            " Pilihan Anda =");
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1){
                        PersegiPanjang.luas();
                    }
                    else if (pilihan2 == 2){
                        PersegiPanjang.keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 2:
                    System.out.println(prompt +
                            " Pilihan Anda =");
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1){
                        segitiga.luas();
                    }
                    else if (pilihan2 == 2){
                        segitiga.keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                case 3:
                    System.out.println(prompt +
                            " Pilihan Anda =");
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1){
                        Trapesium.luas();
                    }
                    else if (pilihan2 == 2){
                        Trapesium.keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;


                case 4 :
                    System.out.println(prompt +
                            " Pilihan Anda =");
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1){
                        Lingkaran.luas();
                    }
                    else if (pilihan2 == 2){
                        Lingkaran.keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;

                case 5 :
                    System.out.println(prompt +
                            " Pilihan Anda =");
                    pilihan2 = input.nextInt();

                    if (pilihan2 == 1){
                        persegi.luas();
                    }
                    else if (pilihan2 == 2){
                        persegi.keliling();
                    }
                    else {
                        System.out.println("Pilihan Tidak Tersedia");
                    }
                    break;
                default:
            }
            System.out.println("Try again? y/n" );
            input.nextLine();
            answer = input.nextLine();
            if (!answer.toLowerCase().equals("y")){
                flag = false;
        }
            }
        }
    }