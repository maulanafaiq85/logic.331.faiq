package Basic;

import java.util.Scanner;

public class PersegiPanjang {
    private static Scanner input = new Scanner(System.in);
    private static int panjang;
    private static int lebar;
    private static int hasil;

    public static void luas() {
        System.out.print("Masukkan panjang : ");
        panjang = input.nextInt();
        System.out.print("Masukkan lebar : ");
        lebar = input.nextInt();
        hasil = panjang * lebar;
        System.out.println("Luas persegi panjang: " + hasil);
    }
    public static void keliling() {
        System.out.print("Masukkan panjang : ");
        panjang = input.nextInt();
        System.out.print("Masukkan lebar : ");
        lebar = input.nextInt();
        System.out.println("Keliling persegi panjang: " + (2 * (panjang + lebar)));
    }
}
