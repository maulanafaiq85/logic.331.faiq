package Basic;

import java.util.Scanner;

public class Trapesium {
    private static Scanner input = new Scanner(System.in);
    private static int alas1;
    private static int alas2;
    private static int tinggi;
    private static int bidang
            ;
    private static int hasil;

    public static void luas() {
        System.out.print("Masukkan Alas A : ");
        alas1 = input.nextInt();
        System.out.print("Masukkan Alas B : ");
        alas2 = input.nextInt();
        System.out.print("Masukkan Tinggi : ");
        tinggi = input.nextInt();
        hasil = (alas1 + alas2) *tinggi / 2;
        System.out.println("Luas Trapesium: " + hasil );
    }
    public static void keliling() {
        System.out.print("Masukkan Alas A : ");
        alas1 = input.nextInt();
        System.out.print("Masukkan Alas B : ");
        alas2 = input.nextInt();
        System.out.print("Masukkan Tinggi : ");
        tinggi = input.nextInt();
        System.out.print("Masukkan Bidang : ");
        bidang = input.nextInt();
        hasil = alas1 + alas2 + tinggi + bidang;
        System.out.println("Luas Trapesium: " + hasil);
    }
}
