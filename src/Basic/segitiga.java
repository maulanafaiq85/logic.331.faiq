package Basic;

import java.util.Scanner;

public class segitiga {
    private static Scanner input = new Scanner(System.in);
    private static int alas;
    private static int tinggi;
    private static int bidang
            ;
    private static int hasil;

    public static void luas() {
        System.out.print("Masukkan Alas : ");
        alas = input.nextInt();
        System.out.print("Masukkan Tinggi : ");
        tinggi = input.nextInt();
        hasil = alas * tinggi / 2;
        System.out.println("Luas segitiga: " + hasil );
    }
    public static void keliling() {
        System.out.print("Masukkan Alas : ");
        alas = input.nextInt();
        System.out.print("Masukkan Tinggi : ");
        tinggi = input.nextInt();
        System.out.print("Masukkan Bidang : ");
        bidang = input.nextInt();
        hasil = alas + tinggi + bidang;
        System.out.println("Keliling segitiga: " + hasil);
    }
}
