package Belajar;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih Soal 1 - 12");
        System.out.println("Masukkan Pilihan Anda");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12){
            System.out.println("Angka Tidak Tersedia");
            pilihan = input.nextInt();
        }
        System.out.println("Masukkan Nilai N");
        n = input.nextInt();
        switch (pilihan){
            case 1:
                Soal1.Resolve(n);
                break;
            case 2:
                Soal2.Resolve(n);
                break;
            case 3:
                Soal3.Resolve(n);
                break;
            case 4:
                Soal4.Resolve(n);
                break;
            case 5:
                Soal5.Resolve(n);
                break;
            case 6:
                Soal6.Resolve(n);
                break;
            case 7:
                Soal7.Resolve(n);
                break;
            case 8:
                Soal8.Resolve(n);
                break;
            case 9:
                Soal9.Resolve(n);
                break;
            case 10:
                Soal10.Resolve(n);
                break;
            case 11:
                Soal11.Resolve(n);
                break;
            case 12:
                break;
        }


    }
}