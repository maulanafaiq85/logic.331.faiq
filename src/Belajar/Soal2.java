package Belajar;

public class Soal2 {
    public static void Resolve (int n){
        int ganjil = 2;
        int [] results = new int[n];

        for (int i = 0; i < n; i++) {
            results [i] = ganjil;
            ganjil += 2;
        }
        Utility.PrintArray1D(results);
    }
}
