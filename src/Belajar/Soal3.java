package Belajar;

public class Soal3 {
    public  static void Resolve(int n){
        int ganjil = 1;
        int [] results = new int[n];

        for (int i = 0; i < n; i++) {
            results[i] = ganjil;
            ganjil += 3;
        }
        Utility.PrintArray1D(results);
    }
}
