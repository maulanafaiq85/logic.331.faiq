package Belajar;

public class Soal5 {
    public static void Resolve(int n){
        int spasi = 3;
        int ganjil = 1;
        int [] results = new int[n];

        for (int i = 0; i < n; i++) {
            if(i % spasi == 2){
                System.out.print("* ");
            }else{
                results [i] = ganjil;
                ganjil += 4;
                System.out.print(results[i] + " ");
            }
        }
    }
}
