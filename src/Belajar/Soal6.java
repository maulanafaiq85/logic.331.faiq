package Belajar;

public class Soal6 {
    public static void Resolve(int n){
        int jarak = 3;
        int ganjil = 1;
        int [] results = new int[n];

        for (int i = 0; i < n; i++) {
            if (i % jarak == 2){
                System.out.print("* ");
                ganjil +=4;
            }else {
                System.out.print(ganjil +" ");
                ganjil += 4;
            }
        }
    }
}
