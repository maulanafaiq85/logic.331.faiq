package Belajar;

public class Soal9 {
    public static void Resolve(int n){
        int jarak = 3;
        int ganjil = 4;
        int [] results = new int[n];

        for (int i = 0; i < n; i++) {
            if(i % jarak == 2){
                System.out.print("* ");
            }else {
                System.out.print(ganjil +" ");
                ganjil *= 4;
            }
        }
    }
}
