package Belajar2;

import java.util.Scanner;

public class Soal11 {
    public static  void Resolve (){
        Scanner input = new Scanner(System.in);
        int data = 0;
        String simbol ="*";

        System.out.println("Masukkan Nilai N");
        int n = input.nextInt();
        String [][] results = new String[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < (n-(i +1)); j++) {
                results [i] [data]=" ";
                data++;
            }
            for (int k = 0; k < i + 1; k++) {
                results [i] [data]=simbol;
                data++;
            }
            data = 0;
        }
        Utility.PrintArrayString2D(results);
    }
}
