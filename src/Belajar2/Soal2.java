package Belajar2;

import java.util.Scanner;

public class Soal2 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int baris1 = 0;
        int baris2 = 1;

        System.out.println("Masukkan Nilai N =");
        int n = input.nextInt();
        int [][] results = new int[2][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0) {
                    results [i][j] = baris1;
                    baris1 += 1;
                }if (i == 1) {
                    if ((j + 1) % 3 == 0){
                        baris2 *= -1;
                        results[i][j] = baris2;
                        baris2 *= -3;
                    }else {
                        results[i][j] = baris2;
                        baris2 *= 3;
                    }
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}

