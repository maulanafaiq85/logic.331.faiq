package Belajar2;

import java.util.Scanner;

public class Soal5 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int data = 0;
        System.out.println("Masukkan Nilai n");
        int n = input.nextInt();
        int baris = 3;
        int[][] results = new int[baris][n];

        for (int i = 0; i < baris; i++) {
            for (int j = 0; j < n; j++) {
                results[i][j] = data;
                data +=1;
            }
        }
        Utility.PrintArray2D(results);
    }
}
