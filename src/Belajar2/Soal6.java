package Belajar2;

import java.util.Scanner;

public class Soal6 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int bilang1 = 0;
        int bilang2 = 1;
        int bilang3 = 0;

        System.out.println("Masukkan Bilangan N ");
        int n = input.nextInt();
        int [][] results = new int[3][n];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0 ){
                    results[i][j] = bilang1;
                    bilang1 ++;
                }else if (i == 1) {
                    results[i][j]=bilang2;
                    bilang2 *= 7;

                }else {
                    bilang3 = (results[0][j] + results[1][j]);
                    results[i][j]=bilang3;
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
