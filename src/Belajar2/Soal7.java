package Belajar2;

import java.util.Scanner;

public class Soal7 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int data = 0;
        int baris = 3;


        System.out.println("Masukkan Bilangan N");
        int n = input.nextInt();
        int [][] results = new int[baris][n];
        int count = 0;

        for (int i = 0; i < baris; i++) {
            for (int j = 0; j < n; j++) {
                if (count % 3 == 0){
                    data *= -1;
                    results[i][j] = data;
                    data *= -1;
                    data += 1;
                    count++;

                }else{
                    results[i][j] = data;
                    data +=1;
                    count++;
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
