package Belajar2;

import java.util.Scanner;

public class Soal8 {
    public static void Resolve(){
        Scanner input = new Scanner(System.in);
        int data1 = 0;
        int data2 =0;
        int baris = 3;
        int bilang1 = 0;

        System.out.println("Masukkan Nilai N");
        int n = input.nextInt();
        int [][] results = new int[baris][n];

        for (int i = 0; i < baris; i++) {
            for (int j = 0; j < n; j++) {
                if(i == 0){
                    results[i][j]=data1;
                    data1 += 1;
                }else if(i == 1){
                    results[i][j]=data2;
                    data2 += 2;
                }else {
                    bilang1 = (results[0][j] + results[1][j]);
                    results [i][j]= bilang1;
                }
            }
        }
        Utility.PrintArray2D(results);
    }
}
