package Belajar2;

public class Utility {
    public static void PrintArray2D(int[][] results){
        for (int i = 0; i < results.length; i++){
            for(int j = 0; j < results[0].length; j++){
                System.out.print(results[i][j] + " ");
            }
            System.out.println();
        }
    }
    public static void PrintArrayString2D(String[][] results){
        for (int i = 0; i < results.length; i++){
            for(int j = 0; j < results[i].length; j++){
                System.out.print(results[i][j]);
            }
            System.out.println();
        }
    }

}
