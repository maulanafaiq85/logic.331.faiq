package DeretAngka;
import java.util.Scanner;
// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 12)");
        System.out.print("Masukkan Pilihan =");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12)
        {
            System.out.print("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        System.out.print("Masukan nilai n: ");
        n = input.nextInt();

        switch (pilihan)
        {
            case 1:
                soal01.Resolve(n);
                break;
            case 2:
                soal02.tambah2(n);
                break;
            case 3:
                soal03.tambah3(n);
                break;
            case 4:
                soal04.tambah4(n);
                break;
            case 5:
                soal05.tambah5(n);
                break;
            case 6:
                soal06.tambah6(n);
                break;
            case 7:
                soal07.tambah7(n);
                break;
            case 8:
                soal08.tambah8(n);
                break;
            case 9:
                soal09.tambah9(n);
                break;
            case 10:
                soal10.tambah10(n);
                break;
            case 11:
                soal11.tambah11(n);
                break;
            case 12:
                soal12.tambah12(n);
                break;
            default:
        }
    }
}