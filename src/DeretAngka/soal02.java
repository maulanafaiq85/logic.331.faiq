package DeretAngka;

public class soal02 {
    public static void tambah2(int n){
        int tambah = 2;
        int [] results = new int[n];

        for(int i = 0; i < n; i++){
            results[i] = tambah;
            tambah += 2;
        }

        Utility.PrintArray1D(results);
    }
}
