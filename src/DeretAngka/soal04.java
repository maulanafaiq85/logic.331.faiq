package DeretAngka;

public class soal04 {
    public static void tambah4(int n){
        int tambah = 1;
        int [] results = new int[n];

        for(int i = 0; i < n; i++){
            results[i] = tambah;
            tambah += 4;
        }

        Utility.PrintArray1D(results);
    }
}