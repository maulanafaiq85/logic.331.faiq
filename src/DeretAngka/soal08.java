package DeretAngka;

public class soal08 {
    public static void tambah8(int n){
        int tambah = 3;
        int [] results = new int[n];

        for(int i = 0; i < n; i++){
            results[i] = tambah;
            tambah *= 3;
        }

        Utility.PrintArray1D(results);
    }
}