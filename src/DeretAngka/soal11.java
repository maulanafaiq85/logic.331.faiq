package DeretAngka;

public class soal11 {
    public static void tambah11(int n) {
        int[] fibonacci = new int[n];


        fibonacci[0] = 1;
        fibonacci[1] = 1;

        for (int i = 2; i < n; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }

        for (int i = 0; i < n; i++) {
            System.out.print(fibonacci[i] + "\t");
        }
    }
}


