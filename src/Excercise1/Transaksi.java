package Excercise1;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Transaksi {
    public static double uang, saldo, sisa;
    public static int setor, bank;
    public static String transaksi, rekening, struk;
    public static Scanner sc = new Scanner(System.in);

    public static void atm() {
        System.out.println ("\n1. Setor Tunai \n2. Transfer");
        System.out.println ("Masukkan Pilihan Anda :");
        int pilih = sc.nextInt();
        switch (pilih) {
            case 1:
                System.out.println ("Masukkan Uang Anda");
                saldo = sc.nextDouble();
                if(saldo >= 25000000){
                    System.out.println("Maaf Anda Telah Melewati Limit Harian");
                }else{
                    System.out.println ("Uang Yang Anda Masukkan Adalah =" + saldo);
                }

                do {
                    System.out.println ("apakah anda ingin melakukan transaksi lain : Ya / Tidak");
                    transaksi = sc.next();

                    if (transaksi.equalsIgnoreCase("ya")) {
                        atm();
                    } else if (transaksi.equalsIgnoreCase("tidak")) {
                        cetak_struk();
                    } else {
                        System.out.println ("Maaf Yang Anda Masukkan Salah");
                    }
                } while (transaksi.equalsIgnoreCase("ya"));

                break;
            case 2:
                System.out.println ("Masukkan Nominal :");
                uang = sc.nextDouble();
                transfer();
                if (uang <= saldo) {
                    saldo = saldo - uang;
                    System.out.println ("Transaksi Berhasil");
                } else {
                    System.out.println ("Saldo Anda Tidak Cukup!");
                }
                break;
            default:
                System.out.println ("Masukkan Anda Salah");
        }
    }
    static void transfer () {
        System.out.println ("Pilih Jenis Transaksi");
        System.out.println ("\n1. Antar Rekening \n2. Antar Bank");
        System.out.println ("Masukkan Pilihan Anda :");
        setor = sc.nextInt();
        if (setor == 1) {
            sisa = saldo - uang;

        } else if (setor == 2) {
            System.out.println ("Pilih Bank");
            System.out.println ("\n1. BRI \n4. BTN");
            System.out.println ("\n2. BCA \n5. BNI");
            System.out.println ("\n3. Mandiri");
            System.out.println ("Masukkan Pilihan Anda :");
            bank = sc.nextInt();
            switch (bank) {
                case 1:
                    System.out.println ("Masukkan No Rekening : ");
                    rekening = sc.next();
                    System.out.println ("Transaksi Berhasil \nRincian Transaksi : ");
                    System.out.println ("Bank : BRI \n No Rekening :" + rekening);
                    System.out.println ("Jumlah Transaksi : " + uang);
                    sisa = saldo - uang;
                    break;
                case 2:
                    System.out.println ("Masukkan No Rekening : ");
                    rekening = sc.next();
                    System.out.println ("Transaksi Berhasil \nRincian Transaksi : ");
                    System.out.println ("Bank : BCA \n No Rekening :" + rekening);
                    System.out.println ("Jumlah Transaksi : " + uang);
                    break;
                case 3:
                    System.out.println ("Masukkan No Rekening : ");
                    rekening = sc.next();
                    System.out.println ("Transaksi Berhasil \nRincian Transaksi : ");
                    System.out.println ("Bank : Mandiri \n No Rekening :" + rekening);
                    System.out.println ("Jumlah Transaksi : " + uang);
                    break;
                case 4:
                    System.out.println ("Masukkan No Rekening : ");
                    rekening = sc.next();
                    System.out.println ("Transaksi Berhasil \nRincian Transaksi : ");
                    System.out.println ("Bank : BTN \n No Rekening :" + rekening);
                    System.out.println ("Jumlah Transaksi : " + uang);
                    break;
                case 5:
                    System.out.println ("Masukkan No Rekening : ");
                    rekening = sc.next();
                    System.out.println ("Transaksi Berhasil \nRincian Transaksi : ");
                    System.out.println ("Bank : BNI \n No Rekening :" + rekening);
                    System.out.println ("Jumlah Transaksi : " + uang);
                    break;
                default:
                    System.out.println ("Yang Anda Masukkan Salah!");
            }

        }


    }
    static void cetak_struk(){
        System.out.println("Apakah Anda Ingin Mencetak Struk : Ya/Tidak");
        struk = sc.next();
        if(struk.equalsIgnoreCase("Ya")){
            System.out.println("Sisa Saldo Anda : " + sisa);
            System.out.println("Terima Kasih Telah Menggunakan Layanan Kami");

        }else{
            System.out.println("Yang Anda Masukkan Salah");
        }

    }
}
