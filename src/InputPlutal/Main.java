package InputPlutal;

import Array2D.*;
import DeretAngka.*;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 12)");
        System.out.print("Masukkan Pilihan =");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12)
        {
            System.out.print("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                Example1.no1(n);
                break;
            case 2:
                example2.no2(n);
                break;
            case 3:
                example3.no2(n);
                break;
            case 4:
                soal04.tambah4(n);
                break;
            case 5:
                example5.no5(n);
                break;
            case 6:
                example6.no6(n);
                break;
            case 7:
                example7.no7(n);
                break;
            case 8:
                example8.no8(n);
                break;
            case 9:

                break;
            case 10:

                break;
            case 11:

                break;
            case 12:

                break;
            default:
        }
    }
}