package InputPlutal;

import java.util.Scanner;

public class example6 {
    public static void no6(int n) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input Deret Angka :");

        String data = input.nextLine();

        int[] nilai = Utility.convert(data);
        int panjang = data.length();
        int baris = 0;
        int[] total = new int[panjang];
        boolean cek = false;

        for (int i = 0; i < panjang; i++) {
            if (i == 0) {
                for (int j = 0; j < panjang; j++) {
                    if (nilai[i] == nilai[j]) {
                        baris++;
                    }
                }
                if (cek) {
                    baris = 0;
                } else {
                    for (int j = 0; j < panjang; j++) {
                        if (nilai[i] == nilai[j]) {
                            baris++;
                        }
                    }
                }
            }
            total[i] = baris;
            System.out.print(baris + "");
            baris = 0;
        }
        System.out.println();
        int output = 0;
        for (int i = 0; i < panjang; i++) {
            output += total[i] / 2;
        }
        System.out.println("Jumlah pasangan bilangan adalah= " + output);
    }
}
