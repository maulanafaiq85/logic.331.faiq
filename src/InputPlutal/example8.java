package InputPlutal;

import java.util.Scanner;

public class example8 {
    public static void no8(int n) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input Deret Angka :");

        String data = input.nextLine();

        int[] nilai = Utility.convert(data);
        int panjang = data.length();
        int nilaibesar = 0 ;
        int besar = 0 ;

        for (int i = 0; i < panjang; i++) {
            if(nilaibesar < nilai[i]){
                nilaibesar = nilai[i];
            }
        }
        for (int i = 0; i < panjang; i++) {
            if(nilaibesar == nilai[i]){
                besar++;
            }
        }
        System.out.println("Nilai Yang Sering Muncul : " + besar);
    }
}

