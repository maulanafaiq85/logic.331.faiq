package Problem_Solving_14;



import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 12)");
        System.out.print("Masukkan Pilihan =");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 14)
        {
            System.out.print("Angka tidak tersedia");
            pilihan = input.nextInt();
        }

        switch (pilihan)
        {
            case 1:
                Soal01.Resolve(n);
                break;
            case 2:
                Soal02.Resolve();
                break;
            case 3:
                Soal03.Resolve();
                break;
            case 4:
                Soal04.Resolve();

                break;
            case 5:
                Soal05.Resolve();

                break;
            case 6:

                break;
            case 7:
                Soal07.Resolve();
                break;
            case 8:
                Soal08.Resolve();

                break;
            case 9:

                break;
            case 10:
                Soal10.Resolve();
                break;
            case 11:
                Soal11.Resolve();
                break;
            case 12:
                soal12.Resolve();
                break;
            case 13:
                Soal13.Resolve();
                break;
            case 14:
                Soal13.Resolve();
                break;
            default:
        }
    }
}