package Problem_Solving_14;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve(int n) {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan N");
        int panjangDeret = input.nextInt();
        int[][] results = new int[2][panjangDeret];

        for (int i = 0; i < 2; i++) {
            System.out.println("Deret Ke" + (i + 1));
            for (int j = 0; j < panjangDeret; j++) {
                if (i == 0) {
                    results[i][j] = (j * 3) - 1;
                    System.out.print(results[0][j] + " ");
                } else if (i == 1) {
                    results[i][j] = (j * (-2)) * 1;
                    System.out.print(results[1][j] + " ");
                }
            }
            System.out.println();
        }
        System.out.println();
        for (int i = 0; i < panjangDeret; i++) {
            System.out.print(results[0][i] + results[1][i] + " ");
        }
    }
}
