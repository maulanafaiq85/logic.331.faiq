package Problem_Solving_14;

import java.util.Scanner;

public class Soal02 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan N");
        String masukkan = input.next();
        String[] tokoArray = masukkan.split("-");//split tanda strip dan dimasukkan ke array
        int[] posisiinput = new int[tokoArray.length];//merubah toko array yang awalnya string ke int

        for (int i = 0; i < tokoArray.length; i++) {
            posisiinput[i] = Integer.parseInt(tokoArray[i]);// merubah data dari string ke int terus di masukkan ke penampung
        }
        double[] jaraktoko = {0, 0.5, 2, 3.5, 5};//dimulai dari nol adalah titik pertama
        //jarak yang total input
        double npindah = 0;
        // jarak antar toko
        double jarak = 0;
        // jarak pada soal (0,5-1,5-2-3,5)
        double jaraksebelumnya = 0;
        // buat ambil posisi data input
        int posisitoko = 0;

        for (int i = 0; i < posisiinput.length; i++) {
            posisitoko = posisiinput[i];//posisi toko mengambil inputan
            jarak = jaraktoko[posisitoko] - jaraksebelumnya;//jarak mengambil nilai jarak pada posisi toko yang kita inputkan dan dikurangi toko yang terakhir dikunjungi
            jaraksebelumnya = jaraktoko[posisitoko];//menggantikan nilai jarak sebelunmya menjadi nilai jarak sekarang untuk ke toko selanjutnya
            npindah += Math.abs(jarak);//total tampungan semua nilai jarak yang telah diinput
        }
        double output = jaraktoko[posisitoko] + npindah;//mengembalikan dari toko terakhir ke titik awal
        double kecepatan = 0.5; //kecepatan 0.5 permenit
        double delaytoko = 10 * posisiinput.length;
        double waktu = (output / kecepatan) + delaytoko;

        System.out.println("nilai = " + waktu + " menit");
    }
}