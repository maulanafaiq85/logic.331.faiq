package Problem_Solving_14;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Soal03 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        // daerah Input
        System.out.println("Masukkan Penjualan");
        String masukkan = input.nextLine();
        String[] dataArray = masukkan.split(",");//split koma
        String[][] dataBuah2d = new String[2][dataArray.length];


        for (int i = 0; i < dataArray.length; i++) {//baris dulu baru kolom
            String[] split = dataArray[i].trim().split(":");//split : dan data dimasukkan ke array
            for (int j = 0; j < split.length; j++) {
                dataBuah2d[j][i] = split[j];
                //[baris 1(nama buah)
                // baris 2 (banyak buah)]
                //menbedakan input nama buah dan banyak buah
            }
        }

        // hashmap
        HashMap<String, Integer> buah = new HashMap<String, Integer>();// menggunakan hash map agar dapat memudahkan memodifikasi jumlahh buah yang sama
        for (int i = 0; i < dataBuah2d[0].length; i++) {//untuk mengolah data dan memasukkan ke hashmap
            String buahcek = dataBuah2d[0][i];
            if (buah.containsKey(buahcek)) {// dibuat melihat apakah data yang kita inputkan ada yang sama didalam hashmap
                int datasebelumnya = buah.get(buahcek);
                int dataambil = Integer.parseInt(dataBuah2d[1][i]);//deklarasi untuk mengambil data buah indek ke 1 yaitu jumlah buah
                int nilaibaru = datasebelumnya + dataambil;
                buah.replace(buahcek, datasebelumnya, nilaibaru);//apabila ada yang sama baru data di replace atau di perbaruhi
            } else {
                int angka = Integer.parseInt(dataBuah2d[1][i]);
                buah.put(buahcek, angka);// apabila buah terindikasi data baru langsung tersimpan ke hashmap
            }
        }
        String[] sortstring = new String[buah.size()];//membuat tampungan untuk dimasukkan ke array dari hashmap
        int index = 0;//untuk memberikan posisi pada array sortstring
        for (String key : buah.keySet()) {//merubah deklarasi nilai key menjadi nama buah
            sortstring[index] = key + ":" + buah.get(key);// mengisi data pada ke 0 menjadi nama buah ditambah : dan ditambah banyak buah dengan cara buah.get berati mengambil nama buah berdasarkan nama buah
            index++;
        }
        Arrays.sort(sortstring);//mengurtkan data yang ada di tampung

        for (int i = 0; i < sortstring.length; i++) {//ngeprint nilai berdasarkan ascending
            System.out.println(sortstring[i]);
        }
    }
}