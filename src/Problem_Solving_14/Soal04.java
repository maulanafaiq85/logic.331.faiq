package Problem_Solving_14;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Uang = ");
        int uang = input.nextInt();
        System.out.println("Massukkan Juamlah Barang = ");
        int barang = input.nextInt();
        System.out.println("Masukkan list barang = ");
        String[][] whislistsplit = new String[2][barang];

        // mengolah input
        for (int i = 0; i < barang; i++) {
            String list = input.nextLine();
            String[] split = list.split(",");

            for (int j = 0; j < split.length; j++) {
                whislistsplit[j][i] = split[j];
            }
        }
        String result = "";
        for (int i = 0; i < barang; i++) {
            String namabarang = whislistsplit[0][i];
            int harga = Integer.parseInt(whislistsplit[1][i]);
            if (uang >= harga) {
                result += namabarang + " ";
                uang -= harga;
            }
        }
        System.out.println(result);
    }

}


//    public static void Resolve() {
//        Scanner input = new Scanner(System.in);
//        List<String>barangbeli = new ArrayList<>();
//        System.out.println("Masukkan Uang Anda = ");
//        double masukkan = input.nextInt();
//        System.out.println("Masukkan Jumlah Barang = ");
//        int jumlahbarang = input.nextInt();
//
//
//        for (int i = 0; i < jumlahbarang; i++) {
//            System.out.println("Nama Barang Ke " + (i + 1) +":");
//            input.nextLine();
//            String cekbarang = input.nextLine();
//            System.out.println("Harga Barang  ");
//            double cekharga = input.nextDouble();
//            if(masukkan >= cekharga){
//                barangbeli.add(cekbarang);
//                masukkan -= cekharga;
//            }
//        }
//        System.out.println("Barang Yang Bisa Di beli ");
//        for(String item : barangbeli){
//            System.out.print(item + " ");
//        }
//    }
//}