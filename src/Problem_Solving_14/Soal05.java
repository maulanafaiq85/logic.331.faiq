package Problem_Solving_14;


import java.util.Scanner;

public class Soal05 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan Jam yg ingin dikonversi");
        System.out.println("Dengan Format JJ:MM AM/PM");
        System.out.println("Contoh = 12:35 AM/ 00:35");
        String inputJam = input.nextLine();

        //int lengthInputJam = inputJam.length();

        String output = "";

        String substringJam = inputJam.substring(0, 2);//untuk ambil subtring jam

        String substringMenit = inputJam.substring(3, 5);// untuk ambil menit dari input

        int konversiSubstringJam = Integer.parseInt(substringJam);

        int konversiSubstringMenit = Integer.parseInt(substringMenit);

        if (inputJam.contains("AM") || inputJam.contains("PM")) {
            if (inputJam.contains("AM")) {
                output = inputJam.substring(0, 5);
            } else if (inputJam.contains("PM")) {
                output = (konversiSubstringJam + 12) + ":" + konversiSubstringMenit;
            }
        } else {
            if (konversiSubstringJam > 12) {
                output = (konversiSubstringJam - 12) + ":" + konversiSubstringMenit + "PM";
            } else {
                output = konversiSubstringJam + ":" + konversiSubstringMenit + "AM";
            }
        }

        System.out.println("Jam Hasil Konversi = " + output);

    }
}


//    public static void Resolve() {
//        Scanner input = new Scanner(System.in);
//        System.out.println("Input kalimat");
//        String kalimat = input.nextLine();
//        String[] kalimatSplit = kalimat.split(" ");
//
//        for (int i = 0; i < kalimatSplit.length; i++) {
//            char [] kalimachar= kalimatSplit[i].toCharArray();
//            for (int j = 0; j < kalimachar.length; j++) {
//                if(j == 0 || j == (kalimachar.length - 1)) {
//                    System.out.print(kalimachar[j]);
//                }
//                else{
//                    System.out.print("*");
//                }
//            }
//            System.out.print(" ");
//        }
//    }
//}
