package Problem_Solving_14;

import java.util.Scanner;

public class Soal07 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);


        System.out.println("Masukkan Pola Lintasan = ");
        String pola = input.nextLine();
        System.out.println("Masukkan Cara Jalan");
        String jalan = input.nextLine();

        char[] polalintasan = pola.toCharArray();
        char[] polaJalan = jalan.toCharArray();

        int lompat = 0;
        int energi = 0;
        String output = "";

        for (int i = 0; i < polaJalan.length; i++) {
            char alop = polalintasan[i + lompat];
            char gerak = polaJalan[i];
            if (gerak == 'w' && alop == '-') {
                energi += 1;
                output = String.valueOf(energi);
            } else if (gerak == 'w' && alop == 'o') {
                output = "JIM KENA MENINGGAL";
                break;
            } else if (gerak == 'j' && (alop == '-' || alop == 'o') && energi >= 2) {
                lompat += 1;
                energi -= 2;
                output = String.valueOf(energi);
            } else if (gerak == 'j' && alop == 'o' && energi < 2) {
                output = "JIM MENINGGAL";
                break;
            }
        }
        System.out.println("Output = " + output);
    }
}
