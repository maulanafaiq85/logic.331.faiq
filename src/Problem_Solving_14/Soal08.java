package Problem_Solving_14;

import java.util.Arrays;
import java.util.Scanner;

public class Soal08 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("+=====+ Andi Belanja +=====+ ");
        System.out.println("Masukkan uang Andi = ");
        int uangAndi = input.nextInt();
        System.out.println("Masukkan Harga kacamata = ");
        System.out.println("Input = ");
        //bug
        input.nextLine();
        String inputKacamata = input.nextLine();
        System.out.println("Pastikan Harga baju = ");
        System.out.println("Input = ");
        String inputBaju = input.nextLine();

        String[] dataKacamata = inputKacamata.split(",");
        String[] dataBaju = inputBaju.split(",");

        int[] dataSum = new int[dataKacamata.length * dataBaju.length];
        int count = 0;

        for (int i = 0; i < dataKacamata.length; i++) {
            for (int j = 0; j < dataBaju.length; j++) {
                int hargaKacamata = Integer.parseInt(dataKacamata[i]);
                int hargaBaju = Integer.parseInt(dataBaju[j]);
                dataSum[count] = hargaKacamata + hargaBaju;
                count++;
            }
        }

        int ambil = 0;
        int sisa = uangAndi - dataSum[0];
        for (int i = 1; i < dataSum.length; i++) {
            if (uangAndi - dataSum[i] <= sisa && dataSum[i] < uangAndi) {
                ambil = dataSum[i];
                sisa = uangAndi - dataSum[i];
            }
        }

        System.out.println("Output = " + ambil);
    }
}
