package Problem_Solving_14;

import java.util.Scanner;

public class Soal10 {
    private static String[] datawadah = {"teko,botol,gelas,cangkir"};

    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("1. Teko");
        System.out.println("2. Botol");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        System.out.println("Pilih Wadah =");
        int pilihan1 = input.nextInt();
        pilihan1 -= 1;
        System.out.println("Masukkan Jumlah =");
        int jumlah = input.nextInt();
        System.out.println("Convert ke");
        System.out.println("1. Teko");
        System.out.println("2. Botol");
        System.out.println("3. Gelas");
        System.out.println("4. Cangkir");
        System.out.println("Masukkan Jumlah =");
        int pilihan2 = input.nextInt();
        switch (pilihan2) {
            case 1:
                konfersiteko(datawadah[pilihan1], jumlah);
                break;

            case 2:
                konfersibotol(datawadah[pilihan1], jumlah);
                break;
            case 3:
                konfersigelas(datawadah[pilihan1], jumlah);
                break;
            case 4:
                konfersicangkir(datawadah[pilihan1], jumlah);
                break;
        }


    }

    private static void konfersiteko(String pilihan1, Integer jumlah) {
        double hasilkonversi = 0;
        String namakonversi = datawadah[0];
        if (pilihan1.equals(datawadah[1])) {
            hasilkonversi = (double) jumlah / 5;
        } else if (pilihan1.equals(datawadah[2])) {
            hasilkonversi = (double) (jumlah / 2) / 5;
        } else if (pilihan1.equals(datawadah[3])) {
            hasilkonversi = ((jumlah / 2.5) / 2) / 5;
        } else {
            hasilkonversi = jumlah;
        }
        System.out.println(jumlah + " " + pilihan1 + " = " + hasilkonversi + " " + namakonversi);
    }

    private static void konfersibotol(String pilihan1, Integer jumlah) {
        double hasilkonfersi = 0;
        String namakonfersi = datawadah[1];
        if (pilihan1.equals(datawadah[0])) {
            hasilkonfersi = jumlah * 5;
        } else if (pilihan1.equals(datawadah[2])) {
            hasilkonfersi = jumlah / 2;
        } else if (pilihan1.equals(datawadah[3])) {
            hasilkonfersi = (jumlah / 2.5) / 2;
        } else {
            hasilkonfersi = jumlah;
        }
        System.out.println("Jumlah" + " " + pilihan1 + " = " + hasilkonfersi + " " + namakonfersi);

    }

    private static void konfersigelas(String pilihan1, int jumlah) {
        double hasilkonversi = 0;
        String namakonversi = datawadah[2];
        if (pilihan1.equals(datawadah[0])) {
            hasilkonversi = (double) (jumlah * 5) * 2;
        } else if (pilihan1.equals(datawadah[1])) {
            hasilkonversi = (double) jumlah * 2;
        } else if (pilihan1.equals(datawadah[3])) {
            hasilkonversi = (jumlah / 2.5);
        } else {
            hasilkonversi = jumlah;
        }
        System.out.println(jumlah + " " + pilihan1 + " = " + hasilkonversi + " " + namakonversi);
    }

    private static void konfersicangkir(String wadah, int jumlah) {
        double hasilkonversi = 0;
        String namakonversi = datawadah[3];
        if (wadah.equals(datawadah[0])) {
            hasilkonversi = ((jumlah * 5) * 2) * 2.5;
        } else if (wadah.equals(datawadah[1])) {
            hasilkonversi = (jumlah * 2) * 2.5;
        } else if (wadah.equals(datawadah[3])) {
            hasilkonversi = jumlah * 2.5;
        } else {
            hasilkonversi = jumlah;
        }
        System.out.println(jumlah + " " + wadah + " = " + hasilkonversi + " " + namakonversi);
    }
}
