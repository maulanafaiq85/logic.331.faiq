package Problem_Solving_14;

import java.util.Scanner;

public class Soal11 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        int poin1 = 0;
        int point2 = 0;
        int point3 = 0;


        System.out.print("Masukkan Pulsa Anda =");
        int n = input.nextInt();

        if (n <= 10000) {
            poin1 = 0;
        }
        if (n > 10000) {
            if (n > 30000) {
                point2 += 20;
            } else {
                point2 = (n - 10000) / 1000;
            }
        }
        if (n > 30000) {
            point3 = ((n - 30000) / 1000) * 2;
        }
        int output = poin1 + point2 + point3;
        System.out.print("Poin Anda Adalah " + poin1 + " + " + point2 + " + " + point3 + " = " + output);
    }
}
