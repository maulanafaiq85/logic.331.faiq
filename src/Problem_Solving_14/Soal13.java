package Problem_Solving_14;

import java.util.Scanner;

public class Soal13 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        char[] huruf = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        System.out.print("Masukkan Huruf");
        String Ihuruf = input.nextLine();
        System.out.print("Masukkan Angka");
        String Iangka = input.nextLine();
        char[] hurufarray = Ihuruf.toCharArray();
        String[] angkaarray = Iangka.split(",");
        String[] output = new String[angkaarray.length];
        for (int i = 0; i < hurufarray.length; i++) {
            int nilai = Integer.parseInt(angkaarray[i]);
            char nilaiAsli = hurufarray[i];
            if (huruf[nilai - 1] == nilaiAsli) {
                output[i] = "True";
            } else {
                output[i] = "False";
            }
        }
        for (int i = 0; i < output.length; i++) {
            if (i == output.length - 1) {
                System.out.print(output[i]);
            } else {
                System.out.print(output[i] + ", ");
            }
        }
    }
}
