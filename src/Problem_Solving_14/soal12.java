package Problem_Solving_14;

import java.util.Scanner;

public class soal12 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Input Uang = ");
        int uang = input.nextInt();
        System.out.println("Massukkan Juamlah Barang = ");
        int barang = input.nextInt();
        System.out.println("Masukkan list barang = ");
        String[][] whislistsplit = new String[2][barang];

        // mengolah input
        for (int i = 0; i < barang; i++) {
            String list = input.nextLine();
            String[] split = list.split(",");

            for (int j = 0; j < split.length; j++) {
                whislistsplit[j][i] = split[j];
            }
        }
        String result = "";
        for (int i = 0; i < barang; i++) {
            String namabarang = whislistsplit[0][i];
            int harga = Integer.parseInt(whislistsplit[1][i]);
            if (uang >= harga) {
                result += namabarang + " ";
                uang -= harga;
            }
        }
        System.out.println(result);
    }
}
