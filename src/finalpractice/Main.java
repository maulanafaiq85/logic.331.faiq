package finalpractice;

import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 10)");
        System.out.print("Masukkan Pilihan =");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 10) {
            System.out.print("Angka tidak tersedia");
            pilihan = input.nextInt();
        }


        switch (pilihan) {
            case 1:
                bambang.Resolve();
                break;
            case 2:
                palindrome.Resolve();
                break;
            case 3:
                rotasi.Resolve();
                break;
            case 4:

                break;
            case 5:

                break;
            case 6:

                break;
            case 7:
                deretangka.Resolve();
                break;
            case 8:

                break;
            case 9:

                break;
            case 10:
                cake.Resolve();

                break;
            default:
        }
    }
}