package finalpractice;

public class Utility {
    public static boolean cek02(String kata) {
        kata = kata.replaceAll("[^A-a-zZ0-9]", " ").toLowerCase();
        int kiri = 0;
        int kanan = kata.length() - 1;

        while (kiri < kanan) {
            if (kata.charAt(kiri) != kata.charAt(kanan)) {
                return false;
            }
            kiri++;
            kanan--;
        }

        return true;
    }
}





