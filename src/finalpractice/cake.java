package finalpractice;

import java.text.DecimalFormat;
import java.util.Scanner;

public class cake {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Cake =");
        int cake = input.nextInt();
        double terigu = (double) 125 / 15;
        double gula = (double) 100 / 15;
        double susu =  (double) 100 / 15;

        double jumlah_terigu = cake * terigu;
        double jumlah_gula = cake * gula;
        double jumlah_susu = cake * susu;


        DecimalFormat df = new DecimalFormat("######");

        System.out.println("Terigu Yang Dibutuhkan = " + " " +  df.format(jumlah_terigu)  + " " + "gr");
        System.out.println("Gula Yang Dibutuhkan = " + " " + df.format(jumlah_gula) + " " + "gr");
        System.out.println("Susu Yang Dibutuhkan = " + " " + df.format(jumlah_susu) + " " + "ml");

    }
}