package finalpractice;

import java.util.Scanner;

public class rotasi {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Angka Yang Ingin dirotasi ");
        String angka = input.nextLine();
        String[] splitAngka = angka.split("");
        int data = 0;
        int[] rotasi = new int[splitAngka.length];
        int[] angkaReal = new int[splitAngka.length];
        for (int i = 0; i < splitAngka.length; i++) {
            angkaReal[i] = Integer.parseInt(splitAngka[i]);
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < angkaReal.length; j++) {
                data = angkaReal[0];
                if (j == angkaReal.length - 1) {
                    rotasi[j] = data;
                } else if (j + 1 > angkaReal.length) {
                    rotasi[j] = angkaReal[angkaReal.length - 1];
                } else {
                    rotasi[j] = angkaReal[j + 1];
                }
            }
            for (int j = 0; j < 3; j++) {
                angkaReal[j] = rotasi[j];
            }
        }
        for (int i = 0; i < angkaReal.length; i++) {
            System.out.print(angkaReal[i]);
        }
    }
}
