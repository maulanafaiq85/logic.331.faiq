package pretest;

import java.util.Scanner;

public class Soal01 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        int ganjil = -1;
        int genap = 0;


        System.out.println("Masukkan Nilai N");
        int n = input.nextInt();
        int[] results = new int[n];

        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < n / 2; j++) {
                if (i == 0) {
                    results[j] = ganjil;
                    ganjil += 2;
                    System.out.print(ganjil + " ");
                } else if (i == 1) {
                    results[j] = genap;
                    genap += 2;
                    System.out.print(genap + " ");
                }
            }
            System.out.println(" ");
        }
    }
}
