package pretest;

import java.util.Scanner;

public class Soal2 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan Kata");
        String kata = input.nextLine();
        kata = kata.replaceAll(" ", "");
        String[] results = kata.split("");
        String ouput_vokal = "";
        String ouput_konsonan = "";

        for (int i = 0; i < kata.length(); i++) {
            if (results[i].equals("a") || results[i].equals("i") || results[i].equals("u") || results[i].equals("e") || results[i].equals("o")) {
                ouput_vokal += results[i];
            } else {
                ouput_konsonan += results[i];
            }
        }
        System.out.println("Output Vokal = " + ouput_vokal);
        System.out.println("Ouput Konsonan = " + ouput_konsonan);
    }
}

