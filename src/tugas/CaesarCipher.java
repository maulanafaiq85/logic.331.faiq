package tugas;

import java.util.Scanner;
public class CaesarCipher {
    public static void no3(int n) {
    Scanner input = new Scanner(System.in);

    System.out.println("Input Kata :");
    String message = input.nextLine();
    int key = 2;

    String encryptedMessage = encrypt(message, key);
    System.out.println("Hasil : " + encryptedMessage);

}

    public static String encrypt(String message, int key) {
        StringBuilder encryptedText = new StringBuilder();

        for (char character : message.toCharArray()) {
            if (Character.isLetter(character)) {
                char base = Character.isUpperCase(character) ? 'A' : 'a';
                char encryptedChar = (char) ((character - base + key) % 26 + base);
                encryptedText.append(encryptedChar);
            } else {
                encryptedText.append(character);
            }
        }

        return encryptedText.toString();
    }

    public static String decrypt(String encryptedMessage, int key) {
        return encrypt(encryptedMessage, 26 - key); // Decrypting is just shifting in the opposite direction
    }
}

