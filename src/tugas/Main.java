package tugas;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 12)");
        System.out.print("Masukkan Pilihan =");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12)
        {
            System.out.print("Angka tidak tersedia");
            pilihan = input.nextInt();
        }


        switch (pilihan)
        {
            case 1:
                CamelCase.no1(n);
                break;
            case 2:
                Louise.no2(n);
                break;
            case 3:
                CaesarCipher.no3(n);
                break;
            case 4:
                MarsExploration.no4(n);
                break;
            case 5:
                HackerRankInAString.no5();
                break;
            case 6:
                Pangrams.no6(n);
                break;
            case 7:

                break;
            case 8:

                break;
            case 9:
                MakingAnagrams.no9(n);
                break;
            case 10:
                TwoStrings.no10(n);
                break;
            default:
        }
    }
}