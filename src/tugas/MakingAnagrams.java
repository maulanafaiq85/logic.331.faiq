package tugas;

import java.util.Scanner;

public class MakingAnagrams {
    public static void no9(int n) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input Kata :");
        String baris1 = input.nextLine();
        String baris2 = input.nextLine();

        int[] alphabet = new int[26];


        for (char c : baris1.toCharArray()) {
            alphabet[c - 'a']++;
        }

        for (char c : baris2.toCharArray()) {
            alphabet[c - 'a']--;
        }

        int delete = 0;

        for (int count : alphabet) {
            delete += Math.abs(count);
        }

        System.out.println("Jumlahnnya Adalah " + delete);
    }
}



