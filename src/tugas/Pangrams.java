package tugas;

import java.util.Scanner;

public class Pangrams {
    public static void no6(int n) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input Kata :");;
        String kata = input.nextLine();
        boolean[] alpabet = new boolean[26];

        kata = kata.toLowerCase();

        for (int i = 0; i < kata.length(); i++) {
            char c = kata.charAt(i);
            if (c >= 'a' && c <= 'z') {
                int index = c - 'a';
                alpabet[index] = true;
            }
        }

        boolean cek = true;
        for (boolean letter : alpabet) {
            if (!letter) {
                cek = false;
                break;
            }
        }

        if (cek) {
            System.out.println("pangram");
        } else {
            System.out.println("not pangram");
        }
    }
}

