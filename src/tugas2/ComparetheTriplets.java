package tugas2;

import java.util.*;
public class ComparetheTriplets {
    public static void no10() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Masukkan Nilai ");

        int[] a = new int[3];
        int[] b = new int[3];

        // Membaca input pertama
        for (int i = 0; i < 3; i++) {
            a[i] = scanner.nextInt();
        }

        // Membaca input kedua
        for (int i = 0; i < 3; i++) {
            b[i] = scanner.nextInt();
        }

        int[] result = compareArray(a, b);

        // Menampilkan hasil
        for (int i = 0; i < 2; i++) {
            System.out.print(result[i] + " ");
        }
    }

    public static int[] compareArray(int[] a, int[] b) {
        int[] result = new int[2];
        for (int i = 0; i < 3; i++) {
            if (a[i] > b[i]) {
                result[0]++;
            } else if (a[i] < b[i]) {
                result[1]++;
            }
        }
        return result;
    }
}


