package tugas2;

import java.util.Scanner;

public class DiagonalDifference {
    public static Scanner sc = new Scanner(System.in);
    public static void no4(int n){
        Scanner input = new Scanner(System.in);
        Scanner scanner = new Scanner(System.in);

        System.out.print("Masukkan Ukuran: ");
        int s = scanner.nextInt();

        int[][] matrix = new int[s][s];

        System.out.println("Masukkan Nilai:");
        for (int i = 0; i < s; i++) {
            for (int j = 0; j < s; j++) {
                matrix[i][j] = scanner.nextInt();
            }
        }

        int diagonal1 = 0;
        int diagonal2 = 0;

        for (int i = 0; i < s; i++) {
            diagonal1 += matrix[i][i];
            diagonal2 += matrix[i][s - 1 - i];
        }

        int total = Math.abs(diagonal1 - diagonal2);

        System.out.print("Total : " + total);
    }
}






