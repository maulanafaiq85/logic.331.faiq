package tugas2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int pilihan = 0;
        int n = 0;

        System.out.println("Pilih soal (1 - 12)");
        System.out.print("Masukkan Pilihan =");
        pilihan = input.nextInt();

        while (pilihan < 1 || pilihan > 12)
        {
            System.out.print("Angka tidak tersedia");
            pilihan = input.nextInt();
        }


        switch (pilihan)
        {
            case 1:
              SolveMeFirst.no1(n);
                break;
            case 2:
                TimeConversion.no2(n);
                break;
            case 3:
                SimpleArraySum.no3(n);
                break;
            case 4:
                DiagonalDifference.no4(n);
                break;
            case 5:
                PlusMinus.no5(n);
                break;
            case 6:
                Staircase.no6();
                break;
            case 7:
                MiniMaxSum.no7();
                break;
            case 8:
                BirthdayCakeCandles.no8();
                break;
            case 9:
                AVeryBigSum.no9(n);
                break;
            case 10:
                ComparetheTriplets.no10();
                break;
            default:
        }
    }
}