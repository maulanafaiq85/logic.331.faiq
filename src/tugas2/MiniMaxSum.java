package tugas2;

import java.util.Scanner;
public class MiniMaxSum {
    public static void no7(){
    Scanner scanner = new Scanner(System.in);
    System.out.print("Enter a list of integers separated by spaces: ");
    String input = scanner.nextLine();


    String[] numbers = input.split(" ");

    int terbesar = 0;
    int terkecil = 0;

        for (String numStr : numbers) {
        int num = Integer.parseInt(numStr);

        if (num % 2 == 0) {
            terbesar += num;
        } else {
            terkecil += num;
        }
    }

        System.out.println("Angka Terkecil : " + terbesar);
        System.out.println("Angka Terbesar: " + terkecil);


}
}

