package tugas2;

import java.util.Scanner;
public class PlusMinus {

    public static void no5(int n){
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the number of elements: ");
    int s = scanner.nextInt();

    int[] arr = new int[s];

        System.out.println("Enter the elements of the array:");
        for (int i = 0; i < s; i++) {
        arr[i] = scanner.nextInt();
    }

    int positif = 0;
    int negatif = 0;
    int zero = 0;

        for (int num : arr) {
        if (num > 0) {
            positif++;
        } else if (num < 0) {
            negatif++;
        } else {
            zero++;
        }
    }

    double positivePro = (double) positif / s;
    double negativPro = (double) negatif / s;
    double zeroPro = (double) zero / s;

        System.out.println("Positive = " + positivePro);
        System.out.println("Negativ = " + negativPro);
        System.out.println("Zero = " + zeroPro);

        scanner.close();
}
}

