package tugas2;


import java.util.Scanner;

public class Staircase {
    public static void no6(){
        Scanner input = new Scanner(System.in);

        System.out.print("Input  :");
        int n = input.nextInt();
        int baris = 0;
        String simbol = "*";

        String[][] results = new String[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < (n-(i+1)); j++) {
                results[i][baris] = " ";
                baris += 1;
            }
            for (int k = 0; k < (i+1); k++) {
                results[i][baris] = simbol;
                baris += 1;
            }
            baris = 0;
        }
        Utillity.PrintArray2DString(results);
    }
}

