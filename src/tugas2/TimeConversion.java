package tugas2;

import java.util.Scanner;
public class TimeConversion {
    public static Scanner sc = new Scanner(System.in);
    public static void no2(int n) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input Waktu :");
        String waktu = input.nextLine(); // Replace this with the time in 12-hour format

        String time24Hour = convertTo24HourFormat(waktu);
        System.out.println("Time in 24-hour format: " + time24Hour);
    }

    public static String convertTo24HourFormat(String waktu) {
        String[] parts = waktu.split(":");
        int hours = Integer.parseInt(parts[0]);
        int minutes = Integer.parseInt(parts[1]);
        int seconds = Integer.parseInt(parts[2].substring(0, 2));
        String amOrPm = parts[2].substring(2);

        if (amOrPm.equals("AM") && hours == 12) {
            hours = 0;
        } else if (amOrPm.equals("PM") && hours != 12) {
            hours += 12;
        }

        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }
}

